class CommentsController < ApplicationController
  before_action :find_commentable
  
  def new
    @comment = Comment.new
  end

  def create
    @comment = @commentable.comments.new(comment_params)
    if @comment.save
      redirect_back(fallback_location: root_path)
      flash[:success] = 'Comment successfully created'
    else
      flash[:error] = 'Something went wrong'
      render 'new'
    end
  end

  def destroy
    @comment = @commentable.comments.find(params[:id])
    if @comment.destroy
      flash[:success] = 'comment was successfully deleted.'
      redirect_back(fallback_location: root_path)
    else
      flash[:error] = 'Something went wrong'
      redirect_to post_path(@post)
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end

  def find_commentable
    if params[:post_id]
      @commentable = Post.find_by_id(params[:post_id])
    elsif params[:comment_id]
      @commentable = Comment.find_by_id(params[:comment_id])
    end
  end
end